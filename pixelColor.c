#include <stdio.h>
#include <stdlib.h>
#include "pixelColor.h"

// Got colour working a bit but it gets really weird especially near the borders

unsigned char stepsToRed (int steps){

   int intensity;

   intensity = (steps*6)%256;

   return intensity;

}

unsigned char stepsToBlue (int steps){

   int intensity;
   intensity = (steps*20)%256;

   return intensity;
}

unsigned char stepsToGreen (int steps){

   int intensity;
   intensity = (steps*15)%256;

   return intensity;
}