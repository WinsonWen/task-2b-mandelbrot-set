/*
 *  mandelbrot.c
 *  Serves images of mandelbrot set
 *
 *  Created by Tony Tran, Winson Wen, Yuan Haojun.
 *  Containing code created by Tim Lambert on 02/04/12.
 *  Containing code created by Richard Buckland on 28/01/11.
 *  Copyright 2012 Licensed under Creative Commons SA-BY-NC 3.0.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <math.h>
#include "mandelbrot.h"
#include "pixelColor.h"

#define SERVER_VERSION 1.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 2165
#define NUMBER_OF_PAGES_TO_SERVE 100000

#define BYTES_PER_PIXEL 3
#define BITS_PER_PIXEL (BYTES_PER_PIXEL*8)
#define NUMBER_PLANES 1
#define PIX_PER_METRE 2835
#define MAGIC_NUMBER 0x4d42
#define NO_COMPRESSION 0
#define OFFSET 54
#define DIB_HEADER_SIZE 40
#define NUM_COLORS 0
#define SIZE 512

typedef unsigned char  bits8;
typedef unsigned short bits16;
typedef unsigned int   bits32;

unsigned char stepsToBlue (int steps);
unsigned char stepsToRed (int steps);
unsigned char stepsToGreen (int steps);

static int waitForConnection (int serverSocket);
static int makeServerSocket (int portno);
static void serveHTML (int socket);
static void serveBMP (int socket, char *request);
static void drawBMP (double x, double y, int z, int socket);
static void writeHeader (int socket);
static void parser (double *x, double *y, int *z, char *request);

int main (int argc, char *argv[]) {

   printf ("************************************\n");
   printf ("Starting mandelbrot server %f\n", SERVER_VERSION);

   int serverSocket = makeServerSocket (DEFAULT_PORT);
   printf ("Access this server at http://localhost:%d/\n", DEFAULT_PORT);
   printf ("************************************\n");

   char request[REQUEST_BUFFER_SIZE];

   int numberServed = 0;
   while (numberServed < NUMBER_OF_PAGES_TO_SERVE) {

      printf ("*** So far served %d pages ***\n", numberServed);

      // wait for a request to be sent from a web browser, open a new
      // connection for this conversation
      int connectionSocket = waitForConnection (serverSocket);

      // read the first line of the request sent by the browser
      int bytesRead;
      bytesRead = read (connectionSocket, request, (sizeof request)-1);

      assert (bytesRead >= 0);
      // were we able to read any data from the connection?

      // print entire request to the console
      printf ("*** Received http request ***\n%s\n", request);

      // send the browser a response using http
      printf ("*** Sending http response ***\n");

      // check the GET request to see if it's requesting the root url
      if (request[5] == ' ') {
         serveHTML(connectionSocket);
      } else {
         serveBMP(connectionSocket, request);
      }

      // close the connection after sending the page- keep aust beautiful
      close(connectionSocket);

      numberServed++;
   }

   // close the server connection after we are done- keep aust beautiful
   printf ("** shutting down the server **\n");
   close (serverSocket);

   return EXIT_SUCCESS;
}

static void serveHTML (int socket) {

   char* message;

   // first send the http response header
   message = "HTTP/1.0 200 OK\n"
             "Content-Type: text/html\n"
             "\n";
   printf ("about to send=> %s\n", message);
   write (socket, message, strlen (message));

   // load the viewer
   message =
      "<!DOCTYPE html>\n"
      "<script src=\"http://almondbread.cse.unsw.edu.au/tiles.js\"></script>"
      "\n";
   write (socket, message, strlen (message));
}

static void serveBMP (int socket, char *request) {

   assert (sizeof(bits8)  == 1);
   assert (sizeof(bits16) == 2);
   assert (sizeof(bits32) == 4);

   char* message;

   // first send the http response header

   // (if you write stings one after another like this on separate
   // lines the c compiler kindly joins them togther for you into
   // one long string)
   message = "HTTP/1.0 200 OK\n"
             "Content-Type: image/bmp\n"
             "\n";
   printf ("about to send=> %s\n", message);
   write (socket, message, strlen (message));

   // now send the BMP
   writeHeader (socket);

   double x = 0;
   double y = 0;
   int z = 0;

   // find the coordinates of the tile requested
   parser (&x, &y, &z, request);

   // draw the BMP
   drawBMP(x, y, z, socket);

}


// start the server listening on the specified port number
static int makeServerSocket (int portNumber) {

   // create socket
   int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
   assert (serverSocket >= 0);
   // error opening socket

   // bind socket to listening port
   struct sockaddr_in serverAddress;
   memset ((char *) &serverAddress, 0,sizeof (serverAddress));

   serverAddress.sin_family      = AF_INET;
   serverAddress.sin_addr.s_addr = INADDR_ANY;
   serverAddress.sin_port        = htons (portNumber);

   // let the server start immediately after a previous shutdown
   int optionValue = 1;
   setsockopt (
      serverSocket,
      SOL_SOCKET,
      SO_REUSEADDR,
      &optionValue,
      sizeof(int)
   );

   int bindSuccess =
      bind (
         serverSocket,
         (struct sockaddr *) &serverAddress,
         sizeof (serverAddress)
      );

   assert (bindSuccess >= 0);
   // if this assert fails wait a short while to let the operating
   // system clear the port before trying again

   return serverSocket;
}

// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
static int waitForConnection (int serverSocket) {

   // listen for a connection
   const int serverMaxBacklog = 10;
   listen (serverSocket, serverMaxBacklog);

   // accept the connection
   struct sockaddr_in clientAddress;
   socklen_t clientLen = sizeof (clientAddress);
   int connectionSocket =
      accept (
         serverSocket,
         (struct sockaddr *) &clientAddress,
         &clientLen
      );

   assert (connectionSocket >= 0);
   // error on accept

   return (connectionSocket);
}

int escapeSteps (double x, double y) {

   int escapeSteps = 0;
   double x1 = 0;
   double x2 = 0;
   double y1 = 0;
   double y2 = 0;

   // mandelbrot math stuff
   while (sqrt(x1 * x1 + y1 * y1) < 2 && escapeSteps < 256) {
      x2 = x1 * x1 - y1 * y1 + x;
      y2 = 2 * x1 * y1 + y;
      x1 = x2;
      y1 = y2;
      escapeSteps++;
   }

   return escapeSteps;
}

static void drawBMP (double x, double y, int z, int socket) {

   int row = 0;
   int col = 0;
   double currentX = 0;
   double currentY = 0;

   // distance per pixel
   double dpp = 0;

   bits8 blue = 0;
   bits8 green = 0;
   bits8 red = 0;

   dpp = pow(2, (-z));

   // finds the coordinate of the left-most and bottom-most pixel
   x = x - ((SIZE / 2) - 0.5) * dpp;
   y = y - ((SIZE / 2) - 0.5) * dpp;

   while (row < SIZE) {
      currentY = y + dpp * row;

      while (col < SIZE) {
         currentX = x + dpp * col;

         blue = stepsToBlue(escapeSteps (currentX, currentY));
         green = stepsToGreen(escapeSteps (currentX, currentY));
         red = stepsToRed(escapeSteps (currentX, currentY));

         write (socket, &blue, sizeof(blue));
         write (socket, &green, sizeof(green));
         write (socket, &red, sizeof(red));

         col++;
      }
      col = 0;
      row++;
   }
}

// writes header for BMP file
static void writeHeader (int socket) {

   assert(sizeof (bits8) == 1);
   assert(sizeof (bits16) == 2);
   assert(sizeof (bits32) == 4);

   bits16 magicNumber = MAGIC_NUMBER;
   write (socket, &magicNumber, sizeof magicNumber);

   bits32 fileSize = OFFSET + (SIZE * SIZE * BYTES_PER_PIXEL);
   write (socket, &fileSize, sizeof fileSize);

   bits32 reserved = 0;
   write (socket, &reserved, sizeof reserved);

   bits32 offset = OFFSET;
   write (socket, &offset, sizeof offset);

   bits32 dibHeaderSize = DIB_HEADER_SIZE;
   write (socket, &dibHeaderSize, sizeof dibHeaderSize);

   bits32 width = SIZE;
   write (socket, &width, sizeof width);

   bits32 height = SIZE;
   write (socket, &height, sizeof height);

   bits16 planes = NUMBER_PLANES;
   write (socket, &planes, sizeof planes);

   bits16 bitsPerPixel = BITS_PER_PIXEL;
   write (socket, &bitsPerPixel, sizeof bitsPerPixel);

   bits32 compression = NO_COMPRESSION;
   write (socket, &compression, sizeof compression);

   bits32 imageSize = (SIZE * SIZE * BYTES_PER_PIXEL);
   write (socket, &imageSize, sizeof imageSize);

   bits32 hResolution = PIX_PER_METRE;
   write (socket, &hResolution, sizeof hResolution);

   bits32 vResolution = PIX_PER_METRE;
   write (socket, &vResolution, sizeof vResolution);

   bits32 numColors = NUM_COLORS;
   write (socket, &numColors, sizeof numColors);

   bits32 importantColors = NUM_COLORS;
   write (socket, &importantColors, sizeof importantColors);
}

// parses information from BMP name
static void parser (double *x, double *y, int *z, char *request) {

   int i = 0;

   // stores if ordinate negative
   int xNegative = 0;
   int yNegative = 0;
   int zNegative = 0;

   int doneX = 0;
   int doneY = 0;
   int doneZ = 0;

   // stores decimal place of the ordinate
   int xDecimal = 0;
   int yDecimal = 0;

   while (i < REQUEST_BUFFER_SIZE) {

      // parses x coordinate
      if (request[i] == 'x' && doneX == 0) {
         i++;
         while (request[i] != '_') {
            if (request[i] >= '0' && request[i] <= '9'
               && xDecimal == 0) {
               *x = *x * 10 + request[i] - 48;
            } else if (request[i] == '-') {
               xNegative = 1;
            } else if (request[i] == '.') {
               xDecimal = 1;
            } else if (request[i] >= '0' && request[i] <= '9'
               && xDecimal != 0) {
               *x = *x + ((request[i] - 48) / pow(10, xDecimal));
               xDecimal++;
            }
            i++;
         }
         if (xNegative == 1) {
            *x = *x * -1;
         }
         doneX = 1;
      }

      // parses y coordinate
      if (request[i] == 'y' && doneY == 0) {
         i++;
         while (request[i] != '_') {
            if (request[i] >= '0' && request[i] <= '9'
               && yDecimal == 0) {
               *y = *y * 10 + request[i] - 48;
            } else if (request[i] == '-') {
               yNegative = 1;
            } else if (request[i] == '.') {
               yDecimal = 1;
            } else if (request[i] >= '0' && request[i] <= '9'
               && yDecimal != 0) {
               *y = *y + ((request[i] - 48) / pow(10, yDecimal));
               yDecimal++;
            }
            i++;
         }
         if (yNegative == 1) {
            *y = *y * -1;
         }
         doneY = 1;
      }

      // parses zoom level
      if (request[i] == 'z' && doneZ == 0) {
         i++;
         while (request[i + 1] != 'b') {
            if (request[i] >= '0' && request[i] <= '9') {
               *z = *z * 10 + request[i] - 48;
            } else if (request[i] == '-') {
               zNegative = 1;
            }
            i++;
         }
         if (zNegative == 1) {
            *z = *z * -1;
         }
         doneZ = 1;
      }

      i++;
   }
}
